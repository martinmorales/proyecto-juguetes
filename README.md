Proyecto Construcción de Juguetes
Taller Las FLORES – Primavera del 2019



¿Qué?

Construcción de Juguetes que permitan ser utilizados (que interactuen) con niños con distintos problemas de motricidad y otras patologías.


¿Para quién?
Niños Hospitalizados específicamente con discapacidad motora “hemiplejía". Internados en el hospital de niños

¿Por qué?
Estimular a partir de:
 trabajo intelectual:dibujos ,sonidos,identificacion  de colores y figuras,que se relacionen con los dispositivos  a partir del juego y de esa forma intentar generar las condiciones para   mejorar su desarrollo en la estadía hospitalaria, ademas poder mostrar nuestros conocimientos
producidos a partir de las investigaciones del taller.

