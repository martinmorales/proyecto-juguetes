// Sweep
// by BARRAGAN <http://barraganstudio.com> 
// This example code is in the public domain.
// Este es el programa para mover un brazo y hacer burbujas
//las conexion de cables del motor servo son: tenemos tres colores uno blanco,negro y rojo, 
//el cable blanco va conectado a pin 9 el rojo va conectado a 5 vol y el negro va conectado a gnd del arduino/uno.

#include <Servo.h> 
 
Servo myservo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 
 
int pos = 0;    // variable to store the servo position 
 
void setup() 
{ 
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object 
} 
 
 
void loop() // repeticion de ciclos 
{ 
  for(pos = 50; pos < 100; pos += 3)  // el motor se mueve desde 40 a 120 
  {                                  // in steps of 1 degree 
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(35);                       // waits 15ms for the servo to reach the position 
  } 
  delay (2000);                      // frena un segundo en 120 grados a la espera del viento
  for(pos = 100; pos>=50; pos-=3)     // el motor vuelve desde la posicion  120 a 40
  {                                
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(35);                       // waits 15ms for the servo to reach the position 
  }
 delay (2000);
} 
